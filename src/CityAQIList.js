import React from "react";
import { format } from "date-fns";

import "./CityAQIList.css";
export const CityAQIList = ({
  aqiData,
  handleRowSelect,
  handleCityMagnifyClick,
  selectedCities,
}) => {
  const getAQICalss = (aqi) => {
    if (aqi <= 50) {
      return "aqi-good";
    }
    if (aqi > 50 && aqi <= 100) {
      return "aqi-satisfactory";
    }
    if (aqi > 100 && aqi <= 200) {
      return "aqi-moderate";
    }
    if (aqi > 200 && aqi <= 300) {
      return "aqi-poor";
    }
    if (aqi > 300 && aqi <= 400) {
      return "aqi-verypoor";
    }
    if (aqi > 400 && aqi <= 500) {
      return "aqi-severe";
    }
    return "";
  };

  const getLastUpdated = (city) => {
    const diffInMs = Math.abs(new Date() - city.lastUpdate);
    const diffInSec = diffInMs / 1000;

    if (diffInSec < 60) {
      return "Few seconds ago";
    }
    if (diffInSec > 60 && diffInSec < 60 * 60) {
      const minutes = Math.floor(diffInSec / 60);
      if (minutes === 1) {
        return "A minute ago";
      } else return `${minutes} minutes ago`;
    }

    if (diffInSec > 60 * 60) {
      return format(city.lastUpdate, "hh:mm aa");
    }

    return "";
  };

  const handleChange = (row) => (e) => {
    handleRowSelect({ city: row, selected: e.target.checked });
  };

  return (
    <table>
      <thead>
        <tr>
          <th>Select</th>
          <th>City</th>
          <th>Current AQI</th>
          <th>Last Updated</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {aqiData.map((row) => (
          <tr key={row.city}>
            <td>
              <input
                checked={
                  selectedCities.findIndex((c) => c.city === row.city) > -1
                }
                onChange={handleChange(row)}
                type="checkbox"
              />
            </td>
            <td>{row.city}</td>
            <td className={getAQICalss(row.aqi)}>{row.aqi}</td>
            <td>{getLastUpdated(row)}</td>
            <td>
              <button onClick={() => handleCityMagnifyClick(row)}>
                Magnified Chart
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
