import "./App.css";
import { CityAQIPage } from "./CityAQI";

function App() {
  return (
    <div className="App">
      <CityAQIPage />
    </div>
  );
}

export default App;
