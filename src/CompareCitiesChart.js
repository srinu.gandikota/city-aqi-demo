import React from "react";
import {
  BarChart,
  CartesianGrid,
  YAxis,
  XAxis,
  Tooltip,
  Legend,
  Bar,
} from "recharts";

export const CompareCitiesChart = ({ citiesChartData, handleClose }) => {
  return (
    <>
      <h1>Compare Cities</h1>
      <BarChart width={730} height={250} data={citiesChartData}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="city" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="aqi" fill="#8884d8" />
      </BarChart>
      <button onClick={handleClose}>close</button>
    </>
  );
};
