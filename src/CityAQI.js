import React from "react";
import { CityAQIList } from "./CityAQIList";
import _ from "underscore";

// import { w3cwebsocket as W3CWebSocket } from "websocket";
import { CompareCitiesChart } from "./CompareCitiesChart";
import { CityMagnifiedChart } from "./CityMagnifiedChart";
import "./CityAQI.css";
import { format } from "date-fns";

const cityData = [
  "Hyderabad",
  "Banglore",
  "Chennai",
  "Pune",
  "Mumbai",
  "Delhi",
  "Calcutta",
  "Indoor",
  "Gurgon",
  "Trivendram",
  "Ahmedabad",
  "Jaipur",
];

// const client = new W3CWebSocket("ws://city-ws.herokuapp.com/");
export class CityAQIPage extends React.Component {
  state = {
    aqiData: [],
    selectedCities: [],
    citiesChartData: [],
    showCitiesComparisionChart: false,
    selectedMagnifiedCity: undefined,
    showCityMaginifiedChart: false,
    cityMagnifiedChartData: [],
  };

  generateRandomData = () => {
    const randomCities = _.sample(
      cityData,
      Math.floor(Math.random() * cityData.length)
    );

    return randomCities.map((city) => ({
      city,
      aqi: Math.random() * 500,
    }));
  };

  componentDidMount() {
    const data = this.generateRandomData();
    this.onReceiveAQIData(data);
    const interval = setInterval(() => {
      const data = this.generateRandomData();
      this.onReceiveAQIData(data);
    }, 1000 * 50);
    return () => clearInterval(interval);
  }

  onReceiveAQIData = (data) => {
    const { aqiData } = this.state;

    let copy = [...aqiData];
    data.forEach((city) => {
      const cityIndex = copy.findIndex((it) => it.city === city.city);
      if (cityIndex < 0) {
        // push new city
        copy.push({
          ...city,
          aqi: Number(city.aqi).toFixed(2),
          lastUpdate: new Date(),
        });
      } else {
        // update time for the existing city
        const obj = copy[cityIndex];
        copy[cityIndex] = {
          ...obj,
          aqi: Number(city.aqi).toFixed(2),
          lastUpdate: new Date(),
        };
      }
    });
    if (this.state.selectedMagnifiedCity) {
      const updated = data.find(
        (it) => it.city === this.state.selectedMagnifiedCity.city
      );
      if (updated) {
        const time = format(new Date(), "hh:mm aa");
        const cityMagnifiedChartData = this.state.cityMagnifiedChartData;
        // not showing more than 10 in chart
        if (cityMagnifiedChartData.length === 10) {
          cityMagnifiedChartData.shift();
        }
        const existing = cityMagnifiedChartData.find((it) => it.time === time);
        if (existing) {
          existing.aqi = updated.aqi;
        } else {
          cityMagnifiedChartData.push({ time, aqi: updated.aqi });
        }

        this.setState({ cityMagnifiedChartData: [...cityMagnifiedChartData] });
      }
    }

    this.setState({ aqiData: copy });
  };

  handleRowSelect = (e) => {
    if (!e.selected) {
      this.setState({
        selectedCities: this.state.selectedCities.filter(
          (it) => it.city !== e.city.city
        ),
      });
    } else {
      const cities = this.state.selectedCities;
      cities.push(e.city);
      this.setState({
        selectedCities: cities,
      });
    }
  };

  handleCityMagnifyClick = (city) => {
    this.setState({
      selectedMagnifiedCity: city,
      showCityMaginifiedChart: true,
      cityMagnifiedChartData: [
        { time: format(city.lastUpdate, "hh:mm aa"), aqi: city.aqi },
      ],
    });
  };

  handleCompareClick = () => {
    this.setState({
      citiesChartData: this.state.selectedCities.map((c) => ({
        city: c.city,
        aqi: c.aqi,
      })),
      showCitiesComparisionChart: true,
    });
  };

  handleCloseCompareCitiesChart = () => {
    this.setState({
      citiesChartData: [],
      showCitiesComparisionChart: false,
      selectedCities: [],
    });
  };

  handleMagnifiedChartClose = () => {
    this.setState({
      selectedMagnifiedCity: undefined,
      showCityMaginifiedChart: false,
      cityMagnifiedChartData: [],
    });
  };
  render() {
    const {
      aqiData,
      citiesChartData,
      selectedCities,
      showCitiesComparisionChart,
      cityMagnifiedChartData,
      selectedMagnifiedCity,
    } = this.state;
    return (
      <div className="city-aqi-container">
        <div className="table-section">
          <CityAQIList
            aqiData={aqiData}
            selectedCities={selectedCities}
            handleRowSelect={this.handleRowSelect}
            handleCityMagnifyClick={this.handleCityMagnifyClick}
          />
          {selectedCities.length > 0 ? (
            <button onClick={this.handleCompareClick}>Compare</button>
          ) : null}
        </div>
        <div className="charts-section">
          {showCitiesComparisionChart ? (
            <CompareCitiesChart
              citiesChartData={citiesChartData}
              handleClose={this.handleCloseCompareCitiesChart}
            />
          ) : null}

          {selectedMagnifiedCity && (
            <CityMagnifiedChart
              city={selectedMagnifiedCity}
              chartData={cityMagnifiedChartData}
              handleClose={this.handleMagnifiedChartClose}
            />
          )}
        </div>
      </div>
    );
  }
}
