import React from "react";
import {
  LineChart,
  CartesianGrid,
  YAxis,
  XAxis,
  Tooltip,
  Legend,
  Line,
} from "recharts";

export const CityMagnifiedChart = ({ chartData, city, handleClose }) => {
  return (
    <>
      <h1>{city.city}</h1>
      <LineChart
        width={730}
        height={250}
        data={chartData}
        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="time" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="aqi" stroke="#8884d8" />
      </LineChart>
      <button onClick={handleClose}>close</button>
    </>
  );
};
