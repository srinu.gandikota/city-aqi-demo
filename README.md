# Features
- Live City wise aqi list
- Highligh AQI based on value
- AQI upto 2 demimals
- AQI comparison of different cities
- Magnified chart of city (AQI changes over time. max 10 data points)

# Note
Web socket server not working. implemented with timers

# demo url
https://city-aqi-demo.web.app/
